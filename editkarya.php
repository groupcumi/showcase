<?php
  session_start();
  if(isset($_SESSION["id"])){
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Edit Post</title>
    <link type="text/css" rel="stylesheet" href="style.css">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"> 
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <script src="slideshow.js"></script>
    <script type="text/javascript" src="jquery-2.1.3.min.js"></script>
    <style>
      #slideShowImages { 
        border: 1px gray solid;
        background-color: gray;
      }
      #slideShowImages img { 
        border: 0.8em black solid;
        padding: 3px;
      }   
    </style>
  </head>
  <body>
    <a href="profile.php" id="namaUser"><?php echo $_SESSION['namauser']; ?></a>
    <a href="logout.php" id="logout">Logout</a>
    <br>
    <form action="search.php" method="get" id="search">
      <input type="text" name="search" placeholder="Search"/>
      <button type="submit"><img src="search.png" alt=" " id="searchLogo"/></button><br>
      <input type="radio" name="searchoption" class="searchRadioBtn" value="karya" checked><span class="searchOption">Karya</span>
      <input type="radio" name="searchoption" class="searchRadioBtn" value="user"><span class="searchOption">User</span>
    </form>
    <p id="logo">FTI UKDW Showcase</p>
    <div id="navbar">
      <a href="index.php" class="topmenu">Home</a>
      <a href="discover.php" class="topmenu">Discover</a>
      <a href="upload.php" class="topmenu">Upload</a>
      <a href="about.php" class="topmenu">About</a>
    </div>
    <p id="navigation">
      <a href="index.php" class="navComponent">Home > </a>
      <a href="discover.php" class="navComponent">Upload</a>
      <h2 class="title">Edit Post</h2>
      <?php
        require ("db.php");
        $con=buka_koneksi();
        $id_karya=$_GET["id"];
        $sql="SELECT k.id,k.judul,k.id_kategori,k.deskripsi FROM karya k, kategori c WHERE k.id_kategori=c.id AND k.id=?";
        $stmt=mysqli_prepare($con,$sql);
        mysqli_stmt_bind_param($stmt,"i",$id_karya);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt,$id_karya,$judul,$id_kategori,$deskripsi);
        mysqli_stmt_fetch($stmt);
      ?>
      <form id="formUpload" method="post"action="editproses.php" enctype="multipart/form-data">
        <input type="hidden" name="id_karya" value="<?php echo $id_karya; ?>"></input>
        <p class="formCaption">Title:</p>
      	<input type="text" name="judul" size="65" style="margin-left:68px;" value="<?php echo $judul; ?>"></input><br>
        <p class="formCaption">Kategori:</P>
        <select name="id_kategori" style="margin-left:41px;">
          <?php
            $con=buka_koneksi();
            $sql="SELECT id, jenis FROM kategori";
            $result=mysqli_query($con,$sql);
            while($data=mysqli_fetch_assoc($result)){
              if($data["id"]==$id_kategori){
          ?>
                <option value="<?php echo $data["id"];?>" selected><?php echo ucfirst($data["jenis"]); ?></option>
          <?php
              }
              else{
          ?>
                <option value="<?php echo $data["id"];?>"><?php echo ucfirst($data["jenis"]); ?></option>
          <?php
              }
            }
          ?>
        </select>
        <br>
        <p class="formCaption">Ganti gambar:</p>
        <input type="file" name="gambar" style="margin-left:8px;"></input>
        <br>
        <p class="formCaption">Deskripsi:</p>
        <textarea cols="55" rows="12" name="deskripsi" style="margin-left:34px;"><?php echo $deskripsi; ?></textarea><br>
        <button type="submit" name="submit" id="submitButtonUpload">Submit</button>
      </form>
      <footer>
        <img src="logo.jpg" id="footerLogo"/>
        <table id="bottomTable">
          <tbody>
            <tr>
              <td><a href="about.php" class="bottomMenu">About</a></td>
            </tr>
            <tr>
              <td><a href="contact.php" class="bottomMenu">Contact us</a></td>
            </tr>
            <tr>
              <td><a href="faq.php" class="bottomMenu">FAQ</a></td>
            </tr>
          </tbody>
        </table>
        <div id="sosmed">
          <h4 id="findUs">Find us on:</h4>
          <a href=""><img class="sosmedIcon" src="fbIcon.png"/></a>
          <a href=""><img class="sosmedIcon" src="twitterIcon.png"/></a>
          <a href=""><img class="sosmedIcon" src="instaIcon.ico"/></a>
          <p id="copyright">Copyright &copy;2016 Butterworth Team</p>
        </div>
      </footer>
  </body>
</html>
<?php
  }
  else header("Location: loginfirst.html");
?>