<?php
  session_start();
  if(isset($_SESSION["id"])){
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Edit Profile</title>
		<link type="text/css" rel="stylesheet" href="style.css">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"> 
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <script src="slideshow.js"></script>
    <script type="text/javascript" src="jquery-2.1.3.min.js"></script>
    <style>
      #slideShowImages { 
        border: 1px gray solid;
        background-color: gray;
      }
      #slideShowImages img { 
        border: 0.8em black solid;
        padding: 3px;
      }   
    </style>
	</head>
	<body>
    <?php
      require("db.php");
    ?>
		<a href="profile.php" id="namaUser"><?php echo $_SESSION['namauser']; ?></a>
		<a href="logout.php" id="logout">Logout</a>
    <br>
    <form action="search.php" method="get" id="search">
      <input type="text" name="search" placeholder="Search"/>
      <button type="submit"><img src="search.png" alt=" " id="searchLogo"/></button><br>
      <input type="radio" name="searchoption" class="searchRadioBtn" value="karya" checked><span class="searchOption">Karya</span>
      <input type="radio" name="searchoption" class="searchRadioBtn" value="user"><span class="searchOption">User</span>
    </form>
		<p id="logo">FTI UKDW Showcase</p>
		<div id="navbar">
			<a href="index.php" class="topmenu">Home</a>
			<a href="discover.php" class="topmenu">Discover</a>
			<a href="upload.php" class="topmenu">Upload</a>
			<a href="about.php" class="topmenu">About</a>
		</div>
    <p id="navigation">
      <a href="index.php" class="navComponent">Home</a> > <a href="profile.php" class="navComponent">Profile</a> > <a href="editprofile.php" class="navComponent">Edit</a>
    </p>
    <h2 class="title">Edit Profile</h2>
    <?php
      $con=buka_koneksi();
      $sql="SELECT nama,alamat,tempat_lahir,tanggal_lahir,gender,no_hp,email FROM user WHERE id=?";
      $stmt=mysqli_prepare($con,$sql);
      mysqli_stmt_bind_param($stmt,"s",$_SESSION["id"]);
      mysqli_stmt_execute($stmt);
      mysqli_stmt_bind_result($stmt,$nama,$alamat,$tempat_lahir,$tanggal_lahir,$gender,$no_hp,$email);
      mysqli_stmt_fetch($stmt);
      mysqli_close($con);
    ?>
    <form id="form" method="post" action="updateuser.php" enctype="multipart/form-data">
      <p class="formCaption">Nama: </p>
      <input type="text" name="nama" style="margin-left:71px" value='<?php echo "$nama"; ?>' size="35"><br>
      <p class="formCaption">Ganti foto:</p>
      <input type="file" name="foto" style="margin-left:43px;"><br>
      <p class="formCaption">Alamat: </p>
      <input type="text" name="alamat" style="margin-left:62px " value='<?php echo "$alamat"; ?>' size="45"><br>
      <p class="formCaption">Tempat lahir: </p>
      <input type="text" name="tempat_lahir" style="margin-left:27px" value='<?php echo "$tempat_lahir"; ?>'><br>
      <p class="formCaption">Tanggal lahir: </p>
      <input type="text" name="tanggal_lahir" style="margin-left:23px" value='<?php echo "$tanggal_lahir"; ?>' placeholder="YYYY/MM/DD"><br>
      <p class="formCaption">Jenis kelamin: </p>
      <select name="jns_kel" style="margin-left:20px">
      <?php
        if($gender==="L"){
      ?>
          <option value="L" selected><?php echo "Laki-laki"; ?></option>
          <option value="P"><?php echo "Perempuan"; ?></option>
      <?php
        }
        else{
      ?>
          <option value="L"><?php echo "Laki-laki"; ?></option>
          <option value="P" selected><?php echo "Perempuan"; ?></option>
      <?php
        }
      ?>
      </select>
      <br>
      <p class="formCaption">No HP: </p>
      <input type="text" name="no_hp" style="margin-left:63px" value='<?php echo "$no_hp"; ?>'><br>
      <p class="formCaption">Email: </p>
      <input type="text" name="email" style="margin-left:68px" value='<?php echo "$email"; ?>' size="30"><br>
      <button type="submit" id="buttonSelesaiEditProfile">Selesai</button>
    </form>
    <footer>
        <img src="logo.jpg" alt=" " id="footerLogo"/>
        <table id="bottomTable">
          <tbody>
            <tr>
              <td><a href="about.php" class="bottomMenu">About</a></td>
            </tr>
            <tr>
              <td><a href="contact.php" class="bottomMenu">Contact us</a></td>
            </tr>
            <tr>
              <td><a href="faq.php" class="bottomMenu">FAQ</a></td>
            </tr>
          </tbody>
        </table>
        <div id="sosmed">
          <h4 id="findUs">Find us on:</h4>
          <a href=""><img class="sosmedIcon" src="fbIcon.png" alt=" " /></a>
          <a href=""><img class="sosmedIcon" src="twitterIcon.png" alt=" " /></a>
          <a href=""><img class="sosmedIcon" src="instaIcon.ico" alt=" " /></a>
          <p id="copyright">Copyright &copy;2016 Butterworth Team</p>
        </div>
    </footer>
  </body>
</html>
<?php
  }
  else{
    header("Location:loginfirst.html");
  }
?>