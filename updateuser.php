<?php
	require("koneksi.php");
	session_start();
	$nama=$_POST["nama"];
	$alamat=$_POST["alamat"];
	$tempat_lahir=$_POST["tempat_lahir"];
	$tanggal_lahir=$_POST["tanggal_lahir"];
	$jns_kel=$_POST["jns_kel"];
	$no_hp=$_POST["no_hp"];
	$email=$_POST["email"];

	if(isset($_SESSION["id"])){
		if($_FILES["foto"]["name"]!=""){
			$gambar = $_FILES["foto"];
			$target_path = "gambar/" . $gambar["name"];
			$ext = pathinfo($target_path,PATHINFO_EXTENSION);
			$check = getimagesize($gambar["tmp_name"]);
    		if($check){
    			if($ext !== "jpg" && $ext !== "JPG" && $ext !== "png" && $ext !== "PNG" && $ext !== "jpeg" && $ext !== "JPEG" && $ext !== "gif" && $ext !== "GIF") 
    				exit("Maaf, hanya bisa JPG, JPEG, PNG & GIF.");
				else{
					if(!move_uploaded_file($gambar["tmp_name"],$target_path))
        				exit("Maaf, upload gagal.");
        		}
			}
			else{
	    		exit("Maaf, file yang anda upload bukan file gambar!");
			}
			$sql="UPDATE user SET nama=?, alamat=?, tempat_lahir=?, tanggal_lahir=?, gender=?, no_hp=?, email=?, path_foto=? WHERE id=?";
			$stmt=mysqli_prepare($sql);
			mysqli_stmt_bind_param($stmt,"sssssssss",$nama,$alamat,$tempat_lahir,$tanggal_lahir,$jns_kel,$no_hp,$email,$target_path,$_SESSION["id"]);
			mysqli_stmt_execute($stmt);
			mysqli_close();
		}
		else{
			$sql="UPDATE user SET nama=?, alamat=?, tempat_lahir=?, tanggal_lahir=?, gender=?, no_hp=?, email=? WHERE id=?";
			$stmt=mysqli_prepare($sql);
			mysqli_stmt_bind_param($stmt,"ssssssss",$nama,$alamat,$tempat_lahir,$tanggal_lahir,$jns_kel,$no_hp,$email,$_SESSION["id"]);
			mysqli_stmt_execute($stmt);
			mysqli_close();
		}
		$_SESSION["namauser"]=$nama;
		if(!$stmt){
			die("Update failed!");
		}
		else{
			header("Location:profile.php");
		}
	}
	else{
		header("Location:loginfirst.html");
	}